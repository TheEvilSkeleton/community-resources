# ![Readme banner](Assets/Banners/repo_banner.svg)
[![Subreddit subscribers](https://img.shields.io/reddit/subreddit-subscribers/fossverse)](https://www.reddit.com/r/FOSSverse/)
[![Discord](https://img.shields.io/discord/704349127865270322?color=5865f2&label=Join%20our%20Discord%21)](https://discord.com/invite/DvFH3Dy)
[![Matrix](https://img.shields.io/badge/Matrix-Join%20our%20Matrix!-00c188)](https://matrix.to/#/#fossverse:mozilla.org)
[![Website](https://img.shields.io/website?down_color=%23F3B5CF&up_color=%239EFFC3&url=https%3A%2F%2Ffossverse.dev%2F)](https://fossverse.dev/)


This is a repository of contents shared within the FOSSverse community. Many of these resources are scattered throughout our various chatting platforms, making them difficult to navigate; this repo is designed to solve that.

> It has a little something for everyone. - IGN

We only want to post quality resources here, and therefore anything contributed should be properly vetted before merging.

Visit [CONTRIBUTING](CONTRIBUTING.md) to find out how to contribute to this repository.

## Table of Contents

### [Android](Android.md) - Curated list of open-source Android apps and resources
### [Applications](Applications.md) - Curated list of open-source desktop applications
### [Articles](Articles.md) - Informative and interesting articles from the open-source community
### [Creative Work](Creative Work.md) - Showcasing the ingenious work of the open-source community
### [Education](Education.md) - Freely accessible learning resources
### [Gaming](Gaming.md) - Curated list of open-source games and resources
### [Research papers](Research Papers.md) - Peer-reviewed research published for everyone

## Licensing
It is not our intention to break any license requirements. Any such incident is purely accidental and we would like to be notified so that we can fix any mistakes regarding license. We try to link all work not created by us and the license below does not apply to them.
  
Contact us on our socials or via email at fossverse@protonmail.com  

---
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>  
The content of the project itself is licensed under <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
