![Creative work banner](Assets/Banners/creative_work_banner.svg)

- [Summary](#summary)

## Summary
Anything creative (e.g. artwork, films, and music) from the open source community that does not fit other categories could go here.