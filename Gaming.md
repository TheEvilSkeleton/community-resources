![gaming](Assets/Banners/gaming_banner.svg)

- [Summary](#summary)
- [Games](#games)
- [Resources](#resources)
  - [Game engines](#game-engines)
  - [Game launchers](#game-launchers)
  - [Game lists](#game-lists)

## Summary
<img src="https://i.imgur.com/kCqH5JJ.png" height=150/>


## Games

- [Mari0](https://github.com/Stabyourself/mari0) ![FOSS badge](Assets/Badges/FOSS_badge.svg)
Mario meets Portal!
- [Mindustry](https://mindustrygame.github.io/) ![FOSS badge](Assets/Badges/FOSS_badge.svg)  
Mindustry is a hybrid tower-defense sandbox factory game. Create elaborate supply chains of conveyor belts to feed ammo into your turrets, produce materials to use for building, and defend your structures from waves of enemies.
- [Minetest](https://github.com/minetest/minetest) ![FOSS badge](Assets/Badges/FOSS_badge.svg)
Minetest is an open source voxel game engine with easy modding and game creation. Has "minecraft-like" game among others.

- [Veloren](https://veloren.net/) ![FOSS badge](Assets/Badges/FOSS_badge.svg)
Multiplayer voxel RPG written in Rust. Very much like cube world, but actively maintained and rapidly improving.
## Resources
### Game engines
- [Minetest](https://github.com/minetest/minetest) ![FOSS badge](Assets/Badges/FOSS_badge.svg)
Minetest is an open source voxel game engine with easy modding and game creation. Has "minecraft-like" game among others.

### Game launchers 
- [Lutris](https://lutris.net/)  ![FOSSBADGE](Assets/Badges/FOSS_badge.svg)  
Lutris is a free and open source game manager. It provides one-click installation of many games via installation scripts from the developers and the community. It greatly simplifies running games on WINE for when Steam is not an option. It supports multiple gaming platforms and selection of emulators.

### Game lists
- [OSGL](https://trilarion.github.io/opensourcegames/) ![OSS badge](Assets/Badges/OSS_badge.svg)
Open Source Games List
