# New applicaton
<!---
To tick a box, add an x inside it, like so:
- [x] I agree that [...]
instead of:
- [ ] I agree that [...]
-->
- [ ] I have read the [Contributing](CONTRIBUTING.md) guidelines
- [ ] I checked if someone already created the same issue

## Application name + link

## Application category
<!--This should be one of the main categories in the repository (ie. Android, Gaming, etc. )-->

## Application description
<!--Brief description of what the application does. You can just copy the description from application page.-->

## Why should it be added?
<!--We would like to see some evidence of use. How long did you use it for, did you run into issues, is it actively developed, etc.-->
